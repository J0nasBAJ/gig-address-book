import * as React from 'react';
import { IAppReduxState } from '../types/global';
import { connect, DispatchProp } from 'react-redux';
import { RouterState } from 'connected-react-router';
import { Route, Switch, Redirect } from 'react-router-dom';

import ErrorView from './Error/ErrorView';
import ContactGridView from './ContactGrid/ContactGridView';
import DeleteContactsView from './DeleteContacts/DeleteContactsView';

interface IConnectedProps extends Partial<RouterState>, DispatchProp<any> {}

const Routes = (props: IConnectedProps) => (
  <Switch location={props.location}>
    <Redirect exact={true} from="/" to="/contacts" />
    <Route path="/contacts" component={ContactGridView} />
    <Route path="/remove-contacts" component={DeleteContactsView} />
    <Route render={ErrorView} />
  </Switch>
);

const mapStateToProps = (state: IAppReduxState): Partial<RouterState> => ({
  location: state.router.location,
});

export default connect(mapStateToProps)(Routes);
