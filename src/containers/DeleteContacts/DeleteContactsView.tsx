import React from 'react';
import EmptyCard from '../../components/cards/EmptyCard';
import ContactCard from '../../components/cards/ContactCard';
import ContactGrid from '../../components/grids/ContactGrid';

import { range } from 'lodash';
import { IContact } from '../../types/contacts';
import { connect, DispatchProp } from 'react-redux';
import { IAppReduxState } from '../../types/global';

interface IState {
  emptyCardCount: number;
}

interface IMappedProps {
  contactIds: string[];
  contacts: { [id: string]: IContact };
}

interface IConnectedProps extends IMappedProps, DispatchProp<any> {}

class DeleteContactsView extends React.Component<IConnectedProps, IState> {
  public state = {
    emptyCardCount: 0,
  };

  public componentWillMount() {
    window.addEventListener('resize', this.handleResize);
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  public componentDidMount() {
    this.handleResize();
  }

  public render() {
    const { contactIds } = this.props;
    const { emptyCardCount } = this.state;
    const emptyCards = range(emptyCardCount);

    return (
      <ContactGrid>
        {contactIds.map(this.renderContactCard)}
        {emptyCards.map(this.renderEmptyCard)}
      </ContactGrid>
    );
  }

  private renderContactCard = (contactId: number | string) => {
    const { contacts } = this.props;
    const contactInfo = contacts[contactId];
    return (
      <ContactCard
        deleteMode={true}
        id={contactInfo.id}
        key={contactInfo.id}
        email={contactInfo.email}
        lastName={contactInfo.lastName}
        firstName={contactInfo.firstName}
        countryCode={contactInfo.countryCode}
        countryName={contactInfo.countryName}
      />
    );
  };

  private renderEmptyCard = (key: number | string) => EmptyCard(key);

  private handleResize = () => {
    const { emptyCardCount } = this.state;

    if (window.innerWidth > 639 && !emptyCardCount) {
      this.setState({ emptyCardCount: 2 });
    } else if (window.innerWidth < 640) {
      this.setState({ emptyCardCount: 0 });
    }
  };
}

const mapStateToProps = (state: IAppReduxState): IMappedProps => ({
  contacts: state.contactState.contacts,
  contactIds: state.contactState.contactIds,
});

export default connect(mapStateToProps)(DeleteContactsView);
