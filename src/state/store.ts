import { config } from '../config';
import { rootSaga } from './sagas';
import { reducers } from './reducers';
import { createBrowserHistory, History } from 'history';
import { IAppReduxState, IStoreConfig } from '../types/global';
import createSagaMiddleware, { SagaMiddleware } from 'redux-saga';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { loadPersistedState } from '../helpers/localStorage';
import {
  Store,
  compose,
  createStore,
  StoreEnhancer,
  applyMiddleware,
} from 'redux';

export function configStore(): IStoreConfig {
  let enhancers: StoreEnhancer<any>;
  let store: Store<IAppReduxState>;
  let sagaMiddleware: SagaMiddleware<any>;

  const middleware = [];
  const persistedState = loadPersistedState();
  const history: History = createBrowserHistory();
  const finalAppReducer = connectRouter(history)(reducers);
  middleware.push(routerMiddleware(history));

  if (config.REACT_APP_ENV !== 'production') {
    // INFO: Development helpers
    const Reactotron = require('reactotron-react-js').default;
    const reactotronRedux = require('reactotron-redux').reactotronRedux;
    const sagaPlugin = require('reactotron-redux-saga');

    Reactotron.configure()
      .use(reactotronRedux())
      .use(sagaPlugin())
      .connect();

    const sagaMonitor = Reactotron.createSagaMonitor();
    sagaMiddleware = createSagaMiddleware({ sagaMonitor });
    middleware.push(sagaMiddleware);
    enhancers = compose(applyMiddleware(...middleware));
    store = Reactotron.createStore(finalAppReducer, persistedState, enhancers);
  } else {
    sagaMiddleware = createSagaMiddleware();
    middleware.push(sagaMiddleware);
    enhancers = compose(applyMiddleware(...middleware));
    store = createStore(finalAppReducer, persistedState, enhancers);
  }

  sagaMiddleware.run(rootSaga);
  return {
    store,
    history,
  };
}
