import { db } from '../../database';
import { call } from 'redux-saga/effects';
import { getUserId } from '../../helpers/localStorage';

function* logUserLAstVisitSaga() {
  try {
    const userId = getUserId();
    yield call(() => db.ref(`activity/${userId}`).set(Date()));
  } catch (error) {
    console.log(error);
  }
}

export { logUserLAstVisitSaga };
