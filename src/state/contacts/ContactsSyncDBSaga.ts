import { keys, uniq } from 'lodash';
import { database } from 'firebase';
import { db } from '../../database';
import { actions } from '../actions';
import { IContact } from '../../types/contacts';
import { put, call, select } from 'redux-saga/effects';
import { contactStateSelector } from './ContactsSelectors';
import DataSnapshot = database.DataSnapshot;

function* syncContactsFromDB() {
  try {
    yield put(actions.contacts.setContactsOnSyncFlagAction(true));
    const data: DataSnapshot = yield call(() =>
      db.ref('contacts').once('value'),
    );
    const { contacts, contactIds } = yield select(contactStateSelector);

    const dbContacts: { [id: string]: IContact } = data.val();
    const dbIds: string[] = keys(dbContacts);

    yield put(
      actions.contacts.setContactsAction({
        contactIds: uniq([...dbIds, ...contactIds]),
        contacts: { ...contacts, ...dbContacts },
      }),
    );
  } catch (error) {
    console.log(error);
  } finally {
    yield put(actions.contacts.setContactsOnSyncFlagAction(false));
  }
}

export { syncContactsFromDB };
