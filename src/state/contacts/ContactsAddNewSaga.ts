import uuid from 'uuid';
import { actions } from '../actions';
import { put, select } from 'redux-saga/effects';
import { contactStateSelector } from './ContactsSelectors';
import { IAddNewContactAction, IContact } from '../../types/contacts';

function* addNewContactSaga(action: IAddNewContactAction) {
  const { payload } = action;

  try {
    yield put(actions.contacts.setContactsOnSyncFlagAction(true));
    const { contacts, contactIds } = yield select(contactStateSelector);
    const contactId = uuid.v1();
    const newContact: IContact = { ...payload, id: contactId };

    yield put(
      actions.contacts.setContactsAction({
        contactIds: [contactId, ...contactIds],
        contacts: { ...contacts, [contactId]: newContact },
      }),
    );
  } catch (error) {
    console.log(error);
  } finally {
    yield put(actions.contacts.setContactsOnSyncFlagAction(false));
  }
}

export { addNewContactSaga };
