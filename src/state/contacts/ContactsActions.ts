import { constants } from '../constants';
import {
  IContact,
  IContactPayload,
  ISetContactsAction,
  ISetContactsPayload,
  IAddNewContactAction,
  IUpdateContactAction,
  IRemoveContactsAction,
  ISyncContactsFromDBAction,
  ISetContactsOnSyncFlagAction,
} from '../../types/contacts';

const setContactsOnSyncFlagAction = (
  flag: boolean,
): ISetContactsOnSyncFlagAction => {
  return {
    type: constants.contacts.SET_ON_SYNC_FLAG,
    flag,
  };
};

const addNewContactAction = (
  payload: IContactPayload,
): IAddNewContactAction => {
  return {
    type: constants.contacts.ADD_NEW_CONTACT,
    payload,
  };
};

const updateContactAction = (payload: IContact): IUpdateContactAction => {
  return {
    type: constants.contacts.UPDATE_CONTACT,
    payload,
  };
};

const removeContactAction = (id: string): IRemoveContactsAction => {
  return {
    type: constants.contacts.REMOVE_CONTACTS,
    id,
  };
};

const setContactsAction = (
  payload: ISetContactsPayload,
): ISetContactsAction => {
  return {
    type: constants.contacts.SET_CONTACTS,
    payload,
  };
};

const syncDBContactsAction = (): ISyncContactsFromDBAction => {
  return {
    type: constants.contacts.SYNC_CONTACTS_FROM_DB,
  };
};

export const contactActions = {
  setContactsAction,
  addNewContactAction,
  updateContactAction,
  removeContactAction,
  syncDBContactsAction,
  setContactsOnSyncFlagAction,
};
