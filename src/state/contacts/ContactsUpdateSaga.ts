import { actions } from '../actions';
import { put, select } from 'redux-saga/effects';
import { contactStateSelector } from './ContactsSelectors';
import { IContact, IUpdateContactAction } from '../../types/contacts';

function* updateContactSaga(action: IUpdateContactAction) {
  const { payload } = action;

  try {
    yield put(actions.contacts.setContactsOnSyncFlagAction(true));
    const { contacts, contactIds } = yield select(contactStateSelector);
    const existingContact = contacts[payload.id];
    if (existingContact) {
      const updatedContact: IContact = { ...payload };

      yield put(
        actions.contacts.setContactsAction({
          contactIds,
          contacts: { ...contacts, [payload.id]: updatedContact },
        }),
      );
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(actions.contacts.setContactsOnSyncFlagAction(false));
  }
}

export { updateContactSaga };
