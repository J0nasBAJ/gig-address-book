import { constants } from '../constants';
import {
  ISetContactsAction,
  IContactsReduxState,
  IClearContactsStateAction,
  ISetContactsOnSyncFlagAction,
} from '../../types/contacts';

const INITIAL_STATE: IContactsReduxState = {
  isOnSync: false,

  contacts: {},
  contactIds: [],
};

type ContactsAction =
  | ISetContactsAction
  | IClearContactsStateAction
  | ISetContactsOnSyncFlagAction;

export const contactsReducer = (
  state: IContactsReduxState = INITIAL_STATE,
  action: ContactsAction,
) => {
  switch (action.type) {
    case constants.contacts.SET_ON_SYNC_FLAG:
      return {
        ...state,
        isOnSync: (action as ISetContactsOnSyncFlagAction).flag,
      };
    case constants.contacts.SET_CONTACTS:
      return {
        ...state,
        ...(action as ISetContactsAction).payload,
      };
    case constants.contacts.CLEAR_CONTACTS_STATE:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
};
