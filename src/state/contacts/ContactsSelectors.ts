import { IAppReduxState } from '../../types/global';

export const contactStateSelector = (state: IAppReduxState) => ({
  contacts: state.contactState.contacts,
  contactIds: state.contactState.contactIds,
});
