import { omit } from 'lodash';
import { actions } from '../actions';
import { put, select } from 'redux-saga/effects';
import { contactStateSelector } from './ContactsSelectors';
import { IRemoveContactsAction } from '../../types/contacts';

function* removeContactSaga(action: IRemoveContactsAction) {
  const { id } = action;

  try {
    yield put(actions.contacts.setContactsOnSyncFlagAction(true));
    const { contacts, contactIds } = yield select(contactStateSelector);
    const remainingContacts = omit(contacts, [id]);
    const remainingIds = contactIds.filter((cid: string) => cid !== id);

    yield put(
      actions.contacts.setContactsAction({
        contactIds: remainingIds,
        contacts: remainingContacts,
      }),
    );
  } catch (error) {
    console.log(error);
  } finally {
    yield put(actions.contacts.setContactsOnSyncFlagAction(false));
  }
}

export { removeContactSaga };
