import { combineReducers, Reducer } from 'redux';
import { IAppReduxState } from '../types/global';
import { modalsReducer } from './modals/ModalReducer';
import { contactsReducer } from './contacts/ContactsReducer';

// INFO: Router reducer is added in store configuration stage
type IPartialAppReduxState = Partial<IAppReduxState>;

export const reducers: Reducer<IPartialAppReduxState> = combineReducers({
  modalState: modalsReducer,
  contactState: contactsReducer,
});
