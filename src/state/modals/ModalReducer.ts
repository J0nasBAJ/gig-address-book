import { constants } from '../constants';
import {
  IModalsReduxState,
  ISetModalVisibleFlagAction,
} from '../../types/modals';

const INITIAL_STATE: IModalsReduxState = {
  addContactIsOpen: false,
};

type ModalAction = ISetModalVisibleFlagAction;

export const modalsReducer = (
  state: IModalsReduxState = INITIAL_STATE,
  action: ModalAction,
) => {
  switch (action.type) {
    case constants.modals.SET_MODAL_VISIBLE_FLAG:
      const { payload } = action;
      const key: string = payload.modalName;
      return { ...state, [key]: payload.visible };
    default:
      return state;
  }
};
