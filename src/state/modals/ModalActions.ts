import { constants } from '../constants';
import {
  ISetModalVisibleFlagAction,
  ISetModalVisibleFlagActionPayload,
} from '../../types/modals';

const setModalVisibleFlagAction = (
  payload: ISetModalVisibleFlagActionPayload,
): ISetModalVisibleFlagAction => {
  return {
    type: constants.modals.SET_MODAL_VISIBLE_FLAG,
    payload,
  };
};

export const modalActions = {
  setModalVisibleFlagAction,
};
