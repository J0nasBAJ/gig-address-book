import { modalActions } from './modals/ModalActions';
import { contactActions } from './contacts/ContactsActions';

export const actions = {
  modal: modalActions,
  contacts: contactActions,
};
