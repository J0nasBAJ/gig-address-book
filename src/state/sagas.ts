import { constants } from './constants';
import { persistState } from '../helpers/localStorage';
import { addNewContactSaga } from './contacts/ContactsAddNewSaga';
import { updateContactSaga } from './contacts/ContactsUpdateSaga';
import { removeContactSaga } from './contacts/ContactsRemoveSaga';
import { logUserLAstVisitSaga } from './user/UserLogActivitySaga';
import { syncContactsFromDB } from './contacts/ContactsSyncDBSaga';
import { contactStateSelector } from './contacts/ContactsSelectors';
import { takeEvery, select, takeLatest, spawn } from 'redux-saga/effects';

function* stateChangeListener() {
  yield spawn(logUserLAstVisitSaga);
  const partialContactsState = yield select(contactStateSelector);
  persistState({ contactState: partialContactsState });
}

function* rootSaga() {
  yield takeEvery('*', stateChangeListener);
  yield takeEvery(constants.contacts.UPDATE_CONTACT, updateContactSaga);
  yield takeEvery(constants.contacts.ADD_NEW_CONTACT, addNewContactSaga);
  yield takeEvery(constants.contacts.REMOVE_CONTACTS, removeContactSaga);
  yield takeLatest(
    constants.contacts.SYNC_CONTACTS_FROM_DB,
    syncContactsFromDB,
  );
}

export { rootSaga };
