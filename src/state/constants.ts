import { modalConstants } from './modals/ModalConstants';
import { contactsConstants } from './contacts/ContactsConstants';

export const constants = {
  modals: modalConstants,
  contacts: contactsConstants,
};
