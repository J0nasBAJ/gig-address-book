import styled from 'styled-components';
import { colors } from '../../helpers/colors';

const ModalContactCard = styled.div`
  width: 60%;
  height: 40vh;
  min-width: 300px;
  min-height: 300px;
  overflow: visible;
  position: relative;
  border-radius: 15px;
  background-color: ${colors.card};
  transition: width 0.4s, height 0.4s;
  box-shadow: 0px 5px 15px rgba(235, 219, 137, 0.8);

  @media (max-width: 660px) {
    width: 80%;
    height: 50vh;
  }
`;

export default ModalContactCard;
