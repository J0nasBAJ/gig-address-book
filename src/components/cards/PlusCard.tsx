import React from 'react';
import PlusButton from '../inputs/PlusButton';
import FlexWrapper from '../wrappers/FlexWrapper';
import ContactWrapper from '../wrappers/ContactWrapper';

import { actions } from '../../state/actions';
import { connect, DispatchProp } from 'react-redux';
import { ISetModalVisibleFlagActionPayload } from '../../types/modals';

const PlusIcon = (fill: string) => (size?: string) => (
  <svg
    fill={fill}
    viewBox="0 0 510 510"
    width={size || '25px'}
    height={size || '25px'}
  >
    <path
      d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z M382.5,280.5h-102v102h-51v-102
			h-102v-51h102v-102h51v102h102V280.5z"
    />
  </svg>
);

interface IConnectedProps extends DispatchProp<any> {}

const PlusCard = (props: IConnectedProps) => {
  const actionObject: ISetModalVisibleFlagActionPayload = {
    modalName: 'addContactIsOpen',
    visible: true,
  };
  const openModal = props.dispatch
    ? () =>
        props.dispatch(actions.modal.setModalVisibleFlagAction(actionObject))
    : undefined;

  return (
    <ContactWrapper>
      <FlexWrapper justify="center" align="center">
        <PlusButton onClick={openModal}>
          {PlusIcon('black')('100px')}
        </PlusButton>
      </FlexWrapper>
    </ContactWrapper>
  );
};

export default connect()(PlusCard);
