import React from 'react';
import ContactWrapper from '../wrappers/ContactWrapper';

const EmptyCard = (key?: number | string) => (
  <ContactWrapper key={key} h={0} o={0} />
);

export default EmptyCard;
