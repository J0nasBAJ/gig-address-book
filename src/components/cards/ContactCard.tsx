import React from 'react';
import EditBar from '../others/EditBar';
import FlexWrapper from '../wrappers/FlexWrapper';
import DefaultInput from '../inputs/DefaultInput';
import DefaultButton from '../inputs/DefaultButton';
import ContactWrapper from '../wrappers/ContactWrapper';

import { omit } from 'lodash';
import { actions } from '../../state/actions';
import { connect, DispatchProp } from 'react-redux';
import { validateCountry, validateEmail } from '../../helpers/validators';

interface IState {
  id: string;
  email: string;
  lastName: string;
  firstName: string;
  editMode: boolean;
  countryName: string;
  countryCode: string;
  saveButtonEnabled: boolean;
}

type StateInputFields = 'email' | 'lastName' | 'firstName';

interface IContactCardProps {
  id: string;
  email: string;
  lastName: string;
  firstName: string;
  countryName?: string;
  countryCode?: string;
  deleteMode?: boolean;
}

interface IConnectedProps extends IContactCardProps, DispatchProp<any> {}

const EditIcon = (fill: string) => (size?: string) => (
  <svg
    fill={fill}
    viewBox="0 0 528 528"
    width={size || '25px'}
    height={size || '25px'}
  >
    <path
      d="M328.883,89.125l107.59,107.589l-272.34,272.34L56.604,361.465L328.883,89.125z M518.113,63.177l-47.981-47.981
		c-18.543-18.543-48.653-18.543-67.259,0l-45.961,45.961l107.59,107.59l53.611-53.611
		C532.495,100.753,532.495,77.559,518.113,63.177z M0.3,512.69c-1.958,8.812,5.998,16.708,14.811,14.565l119.891-29.069
		L27.473,390.597L0.3,512.69z"
    />
  </svg>
);

const ReloadIcon = (fill: string) => (size?: string) => (
  <svg
    fill={fill}
    width={size || '25px'}
    height={size || '25px'}
    viewBox="0 0 342.5 342.5"
  >
    <path
      d="M254.37,22.255c-1.161-0.642-2.53-0.795-3.803-0.428c-1.274,0.367-2.35,1.226-2.992,2.387l-21.758,39.391
    c-1.335,2.417-0.458,5.459,1.96,6.794C264.616,90.748,287.5,129.495,287.5,171.52c0,63.649-51.782,115.431-115.431,115.431
    S56.638,235.169,56.638,171.52c0-23.888,7.557-47.427,21.382-66.897l34.478,34.478c1.338,1.337,3.315,1.806,5.109,1.21
    c1.795-0.596,3.101-2.152,3.374-4.024L139.963,6.271c0.228-1.563-0.295-3.141-1.412-4.258c-1.117-1.117-2.7-1.639-4.258-1.412
    L4.278,19.584c-1.872,0.273-3.428,1.579-4.023,3.374c-0.596,1.795-0.127,3.772,1.21,5.109l37.292,37.292
    C14.788,95.484,1.638,133,1.638,171.52c0,93.976,76.455,170.431,170.431,170.431c93.976,0,170.431-76.455,170.431-170.431
    C342.5,109.478,308.731,52.283,254.37,22.255z"
    />
  </svg>
);

const SaveIcon = (fill: string) => (size?: string) => (
  <svg
    fill={fill}
    width={size || '25px'}
    height={size || '25px'}
    viewBox="0 0 438.533 438.533"
  >
    <path
      d="M432.823,121.049c-3.806-9.132-8.377-16.367-13.709-21.695l-79.941-79.942c-5.325-5.325-12.56-9.895-21.696-13.704
		C308.346,1.903,299.969,0,292.357,0H27.409C19.798,0,13.325,2.663,7.995,7.993c-5.33,5.327-7.992,11.799-7.992,19.414v383.719
		c0,7.617,2.662,14.089,7.992,19.417c5.33,5.325,11.803,7.991,19.414,7.991h383.718c7.618,0,14.089-2.666,19.417-7.991
		c5.325-5.328,7.987-11.8,7.987-19.417V146.178C438.531,138.562,436.629,130.188,432.823,121.049z M182.725,45.677
		c0-2.474,0.905-4.611,2.714-6.423c1.807-1.804,3.949-2.708,6.423-2.708h54.819c2.468,0,4.609,0.902,6.417,2.708
		c1.813,1.812,2.717,3.949,2.717,6.423v91.362c0,2.478-0.91,4.618-2.717,6.427c-1.808,1.803-3.949,2.708-6.417,2.708h-54.819
		c-2.474,0-4.617-0.902-6.423-2.708c-1.809-1.812-2.714-3.949-2.714-6.427V45.677z M328.906,401.991H109.633V292.355h219.273
		V401.991z M402,401.991h-36.552h-0.007V283.218c0-7.617-2.663-14.085-7.991-19.417c-5.328-5.328-11.8-7.994-19.41-7.994H100.498
		c-7.614,0-14.087,2.666-19.417,7.994c-5.327,5.328-7.992,11.8-7.992,19.417v118.773H36.544V36.542h36.544v118.771
		c0,7.615,2.662,14.084,7.992,19.414c5.33,5.327,11.803,7.993,19.417,7.993h164.456c7.61,0,14.089-2.666,19.41-7.993
		c5.325-5.327,7.994-11.799,7.994-19.414V36.542c2.854,0,6.563,0.95,11.136,2.853c4.572,1.902,7.806,3.805,9.709,5.708l80.232,80.23
		c1.902,1.903,3.806,5.19,5.708,9.851c1.909,4.665,2.857,8.33,2.857,10.994V401.991z"
    />
  </svg>
);

const TrashIcon = (fill: string) => (size?: string) => (
  <svg
    fill={fill}
    viewBox="0 0 512 512"
    width={size || '25px'}
    height={size || '25px'}
  >
    <path
      d="M381.621,65.523h-53.943V54.004C327.678,24.226,303.451,0,273.673,0h-35.346c-29.778,0-54.005,24.226-54.005,54.004
		v11.519h-53.943c-34.76,0-63.04,28.28-63.04,63.04v15.975H444.66v-15.976C444.66,93.803,416.38,65.523,381.621,65.523z
		M297.261,65.523H214.74V54.004c0-13.005,10.581-23.587,23.588-23.587h35.346c13.006,0,23.588,10.581,23.588,23.587V65.523z"
    />
    <path
      d="M88.452,174.955v306.112c0,17.056,13.877,30.933,30.934,30.933h273.227c17.057,0,30.934-13.877,30.934-30.933V174.955
		H88.452z M197.532,466.16h-30.417V220.795h30.417V466.16z M271.208,466.16h-30.417V220.795h30.417V466.16z M344.886,466.16
		h-30.417V220.795h30.417V466.16z"
    />
  </svg>
);

class ContactCard extends React.Component<IConnectedProps, IState> {
  public initialContactInfo = {
    id: this.props.id,
    email: this.props.email,
    lastName: this.props.lastName,
    firstName: this.props.firstName,
    countryName: this.props.countryName || '',
    countryCode: this.props.countryCode || '',
  };

  public state = {
    editMode: false,
    saveButtonEnabled: true,
    ...this.initialContactInfo,
  };

  public render() {
    const { deleteMode } = this.props;
    const { email, editMode, lastName, firstName, countryCode } = this.state;
    const disabled = !editMode;

    return (
      <ContactWrapper style={{ animationDelay: `${Math.random() * 0.5}s` }}>
        <FlexWrapper fd="row" justify="flex-start">
          <FlexWrapper p="15px" h="none" flex="1">
            <DefaultInput
              type="text"
              value={firstName}
              readOnly={disabled}
              placeholder="First Name"
              onChange={this.onChange('firstName')}
            />
            <DefaultInput
              type="text"
              value={lastName}
              readOnly={disabled}
              placeholder="Last Name"
              onChange={this.onChange('lastName')}
            />
            <DefaultInput
              type="email"
              value={email}
              readOnly={disabled}
              onChange={this.onChange('email')}
              placeholder="example@address.com"
            />
          </FlexWrapper>
          <EditBar
            h="none"
            w="55px"
            br="0 10px 10px 0"
            m="15px 15px 15px 0"
            bg={deleteMode ? 'red' : undefined}
            hbg={deleteMode ? '2px 2px 20px red' : undefined}
          >
            <FlexWrapper align="center" justify="space-around" flex="1">
              {deleteMode
                ? this.renderDeleteButtons()
                : this.renderEditButtons()}
              <DefaultButton bg="transparent" p="5px">
                {countryCode}
              </DefaultButton>
            </FlexWrapper>
          </EditBar>
        </FlexWrapper>
      </ContactWrapper>
    );
  }

  private renderDeleteButtons = () => {
    return (
      <DefaultButton
        b="none"
        bg="transparent"
        p="5px 4px 2px 5px"
        onClick={this.onDelete}
      >
        {TrashIcon('#000')('18px')}
      </DefaultButton>
    );
  };

  private renderEditButtons = () => {
    const { editMode, saveButtonEnabled } = this.state;
    return (
      <>
        {editMode ? (
          <DefaultButton
            b="none"
            p="5px 4px 2px 5px"
            onClick={this.onSaveEdited}
            disabled={!saveButtonEnabled}
          >
            {SaveIcon('#000')('18px')}
          </DefaultButton>
        ) : (
          <DefaultButton
            b="none"
            p="5px 4px 2px 5px"
            onClick={this.turnOnEditMode}
          >
            {EditIcon('#000')('18px')}
          </DefaultButton>
        )}
        {editMode && (
          <DefaultButton onClick={this.resetState} b="none" p="5px 4px 2px 5px">
            {ReloadIcon('#000')('18px')}
          </DefaultButton>
        )}
      </>
    );
  };

  private onChange = (stateField: StateInputFields) => (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    this.setState(
      {
        ...this.state,
        [stateField]: e.target.value,
      },
      this.validate,
    );
  };

  private turnOnEditMode = () => {
    this.setState({ editMode: true });
  };

  private onSaveEdited = () => {
    const { id, dispatch } = this.props;
    const dispatchIsFn = typeof dispatch === 'function';
    const updatedContactInfo = omit(this.state, [
      'editMode',
      'saveButtonEnabled',
    ]);
    const stringifiedNewInfo = JSON.stringify(updatedContactInfo);
    const stringifiedOldInfo = JSON.stringify(this.initialContactInfo);
    if (stringifiedNewInfo !== stringifiedOldInfo && dispatchIsFn) {
      this.initialContactInfo = { id, ...updatedContactInfo };
      dispatch(actions.contacts.updateContactAction(this.initialContactInfo));
    }
    this.setState({ editMode: false });
  };

  private onDelete = () => {
    const { id, dispatch } = this.props;
    if (typeof dispatch === 'function') {
      dispatch(actions.contacts.removeContactAction(id));
    }
  };

  private validate = () => {
    const {
      firstName,
      lastName,
      countryCode,
      countryName,
      saveButtonEnabled,
    } = this.state;
    const emailIsValid = validateEmail(this.state.email);
    const countryIsValid = validateCountry({ countryCode, countryName });
    const formIsValid = (firstName &&
      lastName &&
      emailIsValid &&
      countryIsValid) as boolean;

    if (saveButtonEnabled !== formIsValid) {
      this.setState({ saveButtonEnabled: formIsValid });
    }
  };

  private resetState = () => {
    this.setState(this.initialContactInfo, this.validate);
  };
}

export default connect()(ContactCard);
