import React from 'react';
import styled from 'styled-components';

interface IContactGrid extends React.HTMLProps<HTMLDivElement> {
  p?: string;
}

const ContactGrid = styled.div`
  width: 100%;
  display: flex;
  flex-flow: wrap;
  max-width: 1280px;
  justify-content: center;
  padding: ${(p: IContactGrid) =>
    typeof p.p !== 'undefined' ? p.p : '50px 0 0 0'};
`;

export default ContactGrid;
