import React from 'react';
import styled from 'styled-components';
import DefaultButton from './DefaultButton';
import OutsideClickWrapper from '../wrappers/OutsideClickWrapper';

import { Link } from 'react-router-dom';
import { colors } from '../../helpers/colors';
import { connect, DispatchProp } from 'react-redux';

const DropdownContent = styled.div`
  z-index: 1;
  width: 30vh;
  display: block;
  margin-top: 3px;
  max-height: 30vh;
  overflow: scroll;
  position: absolute;
  background-color: ${colors.primaryButton};
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);

  .menu-item {
    padding: 10px;
    background-color: ${colors.input};

    &:hover {
      cursor: pointer;
      background-color: ${colors.primaryButton};
    }

    &:active {
      opacity: 0.5;
    }
  }

  a {
    color: inherit;
    text-decoration: inherit;
  }
`;

interface IState {
  contentVisible: boolean;
}

interface IMenuItem {
  link?: string;
  label: string;
  action?: any;
}

interface IDropdownInputProps {
  menuItems: IMenuItem[];
}

interface IConnectedProps extends IDropdownInputProps, DispatchProp<any> {}

class MenuButton extends React.Component<IConnectedProps, IState> {
  public state = {
    contentVisible: false,
  };

  public render() {
    const { menuItems } = this.props;
    const { contentVisible } = this.state;

    return (
      <OutsideClickWrapper onOutsideClick={this.toggleContent()}>
        <div style={{ position: 'relative' }}>
          <DefaultButton onClick={this.toggleContent(!contentVisible)}>
            Menu
          </DefaultButton>
          {contentVisible && (
            <DropdownContent>{menuItems.map(this.renderItems)}</DropdownContent>
          )}
        </div>
      </OutsideClickWrapper>
    );
  }

  private renderItems = (item: IMenuItem, index: number) => {
    return item.link ? (
      <Link key={index} to={item.link}>
        <div className="menu-item" onClick={this.toggleContent()}>
          {item.label}
        </div>
      </Link>
    ) : (
      <div
        key={index}
        className="menu-item"
        onClick={this.syncContacts(item.action)}
      >
        {item.label}
      </div>
    );
  };

  private toggleContent = (open: boolean = false) => () => {
    this.setState({ contentVisible: open });
  };

  private syncContacts = (action: any) => () => {
    const { dispatch } = this.props;
    if (typeof dispatch === 'function') {
      dispatch(action);
      this.toggleContent()();
    }
  };
}

export default connect()(MenuButton);
