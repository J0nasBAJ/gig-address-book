import styled from 'styled-components';
import { colors } from '../../helpers/colors';

const PlusButton = styled.button`
  border: none;
  width: 100px;
  height: 100px;
  display: flex;
  outline: none;
  border-radius: 50%;
  align-items: center;
  justify-content: center;
  background-color: transparent;
  transition: background 0.3s, color 0.3s;

  &:hover {
    svg {
      fill: ${colors.primaryButton};
    }
  }

  &:active {
    opacity: 0.5;
  }
`;

export default PlusButton;
