import React from 'react';
import styled from 'styled-components';
import { colors } from '../../helpers/colors';

interface IDefaultButton extends React.HTMLProps<HTMLInputElement> {
  m?: string;
  p?: string;
  b?: string;
  bg?: string;
  c?: string;
  br?: string;
  transition?: string;
  transform?: 'uppercase' | 'lowercase' | 'capitalize' | 'none';
}

const DefaultButton = styled.button`
  outline: none;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.2em;
  display: inline-block;
  text-decoration: none;

  margin: ${(p: IDefaultButton) => p.m || 'none'};
  padding: ${(p: IDefaultButton) => p.p || '10px'};
  border-radius: ${(p: IDefaultButton) => p.br || '3px'};
  color: ${(p: IDefaultButton) => p.c || colors.buttonFont};
  border: ${(p: IDefaultButton) => p.b || '1px solid #323330'};
  text-transform: ${(p: IDefaultButton) => p.transform || 'none'};
  background-color: ${(p: IDefaultButton) => p.bg || colors.primaryButton};
  transition: ${(p: IDefaultButton) =>
    p.transition || 'background .3s, color .3s'};

  &:enabled:hover {
    svg {
      fill: #f5da55;
    }
    color: #f5da55;
    background-color: #323330;
  }

  &:disabled {
    opacity: 0.5;
  }

  &:active {
    opacity: 0.5;
  }
`;

export default DefaultButton;
