import * as React from 'react';
import styled from 'styled-components';
import { colors } from '../../helpers/colors';

interface IDefaultInput extends React.HTMLProps<HTMLInputElement> {
  m?: string;
  p?: string;
  b?: string;
  bg?: string;
  fc?: string;
  h?: number | string;
  w?: number | string;
  fs?: number | string;
}

const DefaultInput = styled.input`
  outline: none;
  -webkit-appearance: textfield;
  width: ${(p: IDefaultInput) => p.w || 'none'};
  border: ${(p: IDefaultInput) => p.b || 'none'};
  padding: ${(p: IDefaultInput) => p.p || '10px'};
  color: ${(p: IDefaultInput) => p.fc || colors.primaryFont};
  background-color: ${(p: IDefaultInput) => p.bg || colors.input};
  font-size: ${(p: IDefaultInput) =>
    typeof p.fs !== 'undefined' ? p.fs : '14px'};
`;

export default DefaultInput;
