import React from 'react';
import styled from 'styled-components';

interface IAnimatedModalButton extends React.HTMLProps<HTMLButtonElement> {
  p?: string;
  b?: string;
  bg?: string;
  br?: string;
  fs?: string;
  top?: number | string;
  right?: number | string;
}

const AnimatedModalButton = styled.button`
  border: none;
  outline: none;
  font-weight: 400;
  line-height: 1.2em;
  display: inline-block;
  text-decoration: none;

  color: #f5da55;
  position: absolute;
  transition: top 0.6s, right 0.3s;
  top: ${(p: IAnimatedModalButton) => p.top || '20%'};
  padding: ${(p: IAnimatedModalButton) => p.p || '10px'};
  right: ${(p: IAnimatedModalButton) => p.right || '10%'};
  font-size: ${(p: IAnimatedModalButton) => p.fs || '14px'};
  border-radius: ${(p: IAnimatedModalButton) => p.br || '3px'};
  border: ${(p: IAnimatedModalButton) => p.b || '3px solid rgb(235, 219, 137)'};
  background-color: ${(p: IAnimatedModalButton) => p.bg || 'transparent'};

  &:active {
    opacity: 0.5;
  }
`;

export default AnimatedModalButton;
