import React, { ReactNode } from 'react';
import styled from 'styled-components';
import DefaultInput from './DefaultInput';
import OutsideClickWrapper from '../wrappers/OutsideClickWrapper';

import { colors } from '../../helpers/colors';

const DropdownContent = styled.div`
  z-index: 1;
  width: 100%;
  display: block;
  margin-top: 3px;
  max-height: 30vh;
  overflow: scroll;
  position: absolute;
  background-color: ${colors.primaryButton};
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);

  .option {
    padding: 10px;
    background-color: ${colors.input};

    &:hover {
      cursor: pointer;
      background-color: ${colors.primaryButton};
    }

    &:active {
      opacity: 0.5;
    }
  }
`;

interface IState {
  filterValue: string;
  contentVisible: boolean;
}

interface IDropdownInputProps<T> {
  value?: T;
  options: T[];
  defaultValue?: T;
  placeholder?: string;
  onChange?: (v: T | null) => void;
  valueToString: (v?: T) => string;
  renderOptionContent: (v: T) => ReactNode;
}

export default class DropdownInput<T> extends React.Component<
  IDropdownInputProps<T>,
  IState
> {
  public state = {
    filterValue: '',
    contentVisible: false,
  };

  public render() {
    const { filterValue, contentVisible } = this.state;
    const { value, placeholder, defaultValue, valueToString } = this.props;

    const filteredOptions = this.filterOptions();

    return (
      <OutsideClickWrapper onOutsideClick={this.toggleContent()}>
        <div style={{ position: 'relative' }}>
          <DefaultInput
            type="search"
            placeholder={placeholder}
            w="-webkit-fill-available"
            onFocus={this.toggleContent(true)}
            onClick={this.toggleContent(true)}
            onChange={this.onFilterTextChange}
            value={filterValue || valueToString(value)}
            defaultValue={valueToString(defaultValue) || undefined}
          />
          {contentVisible && (
            <DropdownContent>
              {filteredOptions.map(this.renderOptions)}
            </DropdownContent>
          )}
        </div>
      </OutsideClickWrapper>
    );
  }

  private renderOptions = (option: T, index: number) => {
    const { renderOptionContent } = this.props;
    return (
      <div className="option" key={index} onClick={this.onSelect(option)}>
        {renderOptionContent(option)}
      </div>
    );
  };

  private toggleContent = (open: boolean = false) => () => {
    this.setState({ contentVisible: open });
  };

  private onSelect = (value: T) => () => {
    const { onChange } = this.props;
    this.setState({
      filterValue: '',
      contentVisible: false,
    });
    if (typeof onChange === 'function') {
      onChange(value);
    }
  };

  private onFilterTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { onChange } = this.props;
    this.setState({ filterValue: e.target.value });
    if (typeof onChange === 'function' && !e.target.value) {
      onChange(null);
    }
  };

  private filterOptions = () => {
    const { filterValue } = this.state;
    const { options, valueToString } = this.props;
    const lowCaseFilterValue = filterValue.toLowerCase();
    return lowCaseFilterValue
      ? options.filter(o =>
          valueToString(o)
            .toLowerCase()
            .startsWith(lowCaseFilterValue),
        )
      : options;
  };
}
