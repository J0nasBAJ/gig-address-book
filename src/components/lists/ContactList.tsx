import * as React from 'react';
import styled from 'styled-components';

interface IContactList extends React.HTMLProps<HTMLUListElement> {
  p?: string;
}

const ContactList = styled.ul`
  margin: auto;
  list-style: none;
  padding: ${(p: IContactList) =>
    typeof p.p !== 'undefined' ? p.p : '50px 0 0 0'};
`;

export default ContactList;
