import React from 'react';
import styled from 'styled-components';
import DefaultInput from '../inputs/DefaultInput';
import FlexWrapper from '../wrappers/FlexWrapper';
import ModalWrapper from '../wrappers/ModalWrapper';
import DropdownInput from '../inputs/DropdownInput';
import ModalContactCard from '../cards/ModalContactCard';
import AnimatedModalButton from '../inputs/AnimatedModalButton';
import OutsideClickWrapper from '../wrappers/OutsideClickWrapper';

import { capitalize } from 'lodash';
import { actions } from '../../state/actions';
import { colors } from '../../helpers/colors';
import { ICountry } from '../../types/contacts';
import { IAppReduxState } from '../../types/global';
import { connect, DispatchProp } from 'react-redux';
import { countries } from '../../helpers/staticData';
import { validateCountry, validateEmail } from '../../helpers/validators';

const AnimatedModalAddButton = styled(AnimatedModalButton)`
  right: 5%;
  border: none;
  padding: 0 15px;

  &:enabled:hover {
    background-color: transparent;
    svg {
      fill: rgb(101, 173, 67);
    }
  }

  &:disabled {
    opacity: 0.5;
  }

  @media (max-width: 660px) {
    top: 75%;
    right: 52%;
    transition: top 0.3s, right 0.6s;
  }
`;

const AnimatedModalCancelButton = styled(AnimatedModalButton)`
  padding: 15px;
  font-size: 18px;
  border-radius: 35px;

  &:hover {
    background-color: rgb(219, 47, 33);
  }

  @media (max-width: 660px) {
    top: 75%;
    right: 23%;
    transition: top 0.3s, right 0.6s;
  }
`;

const AnimatedFlexWrapper = styled(FlexWrapper)`
  min-height: 215px;
  transition: padding 0.6s;
  padding: 20px 23vh 20px 30px;
  height: -webkit-fill-available;

  @media (max-width: 660px) {
    height: 35vh;
    padding: 1vh 3vh;
  }
`;

const PlusIcon = (fill: string) => (size?: string) => (
  <svg
    fill={fill}
    viewBox="0 0 510 510"
    width={size || '25px'}
    height={size || '25px'}
  >
    <path
      d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z M382.5,280.5h-102v102h-51v-102
			h-102v-51h102v-102h51v102h102V280.5z"
    />
  </svg>
);

interface IState {
  email: string;
  lastName: string;
  firstName: string;
  country: ICountry;
  addDisabled: boolean;
}

interface IMappedProps {
  addContactIsOpen: boolean;
}

interface IConnectedProps extends IMappedProps, DispatchProp<any> {}

class AddContactModal extends React.Component<IConnectedProps, IState> {
  public state = { ...AddContactModal.initialState };
  private static initialState = {
    addDisabled: true,
    email: '',
    lastName: '',
    firstName: '',
    country: {
      countryName: '',
      countryCode: '',
    },
  };

  public render() {
    const { addContactIsOpen } = this.props;
    const { email, country, lastName, firstName, addDisabled } = this.state;

    return !addContactIsOpen ? null : (
      <ModalWrapper closeOnEsc={true} onClose={this.closeModal}>
        <FlexWrapper align="center" justify="center">
          <ModalContactCard>
            <OutsideClickWrapper onOutsideClick={this.closeModal}>
              <AnimatedFlexWrapper>
                <DefaultInput
                  type="text"
                  autoFocus={true}
                  value={firstName}
                  placeholder="First Name"
                  onChange={this.onTextChange('firstName')}
                />
                <DefaultInput
                  type="text"
                  value={lastName}
                  placeholder="Last Name"
                  onChange={this.onTextChange('lastName')}
                />
                <DefaultInput
                  type="email"
                  value={email}
                  placeholder="example@address.com"
                  onChange={this.onTextChange('email')}
                />
                <DropdownInput
                  value={country}
                  placeholder="Country"
                  onChange={this.onCountryChange}
                  options={countries.countriesArray}
                  valueToString={this.countryObjectToString}
                  renderOptionContent={this.renderCountryContent}
                />
              </AnimatedFlexWrapper>
              <AnimatedModalAddButton
                onClick={this.onAdd}
                disabled={addDisabled}
              >
                {PlusIcon(colors.primaryButton)('60px')}
              </AnimatedModalAddButton>
              <AnimatedModalCancelButton
                top="60%"
                right="4.8%"
                onClick={this.closeModal}
              >
                Cancel
              </AnimatedModalCancelButton>
            </OutsideClickWrapper>
          </ModalContactCard>
        </FlexWrapper>
      </ModalWrapper>
    );
  }

  private onTextChange = (fieldName: string) => (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    this.setState(
      {
        ...this.state,
        [fieldName]: e.target.value,
      },
      this.validate,
    );
  };

  private countryObjectToString = (country: ICountry) =>
    country ? country.countryName : '';

  private onCountryChange = (country: ICountry) => {
    this.setState({ country }, this.validate);
  };

  private renderCountryContent = (country: ICountry) => {
    return (
      <FlexWrapper fd="row" align="center" justify="space-between">
        <span>{country.countryName}</span>
      </FlexWrapper>
    );
  };

  private validate = () => {
    const { firstName, lastName, email, country, addDisabled } = this.state;
    const emailIsValid = validateEmail(email);
    const countryIsValid = validateCountry(country);
    const formIsValid = firstName && lastName && emailIsValid && countryIsValid;

    if (formIsValid && addDisabled) {
      this.setState({ addDisabled: false });
    } else if (!formIsValid && !addDisabled) {
      this.setState({ addDisabled: true });
    }
  };

  private closeModal = () => {
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch(
        actions.modal.setModalVisibleFlagAction({
          visible: false,
          modalName: 'addContactIsOpen',
        }),
      );
      this.resetState();
    }
  };

  private onAdd = () => {
    const { dispatch } = this.props;
    const { firstName, lastName, email, country } = this.state;
    if (dispatch) {
      dispatch(
        actions.contacts.addNewContactAction({
          email: email.toLowerCase(),
          lastName: capitalize(lastName),
          firstName: capitalize(firstName),
          ...country,
        }),
      );
      this.closeModal();
      this.resetState();
    }
  };

  private resetState = () => {
    this.setState(AddContactModal.initialState);
  };
}

const mapStateToProps = (state: IAppReduxState): IMappedProps => ({
  addContactIsOpen: state.modalState.addContactIsOpen,
});

export default connect(mapStateToProps)(AddContactModal);
