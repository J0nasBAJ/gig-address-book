import React from 'react';

interface IOutsideClickWrapper {
  onOutsideClick?: () => void;
}

export default class OutsideClickWrapper extends React.PureComponent<
  IOutsideClickWrapper,
  {}
> {
  private outerRef = React.createRef<HTMLDivElement>();

  public componentDidMount() {
    document.addEventListener('mousedown', this.onMouseDown);
  }

  public componentWillUnmount() {
    document.removeEventListener('mousedown', this.onMouseDown);
  }

  public render() {
    const { children } = this.props;
    return <div ref={this.outerRef} children={children} />;
  }

  private onMouseDown = (e: DocumentEventMap['mousedown']) => {
    const { onOutsideClick } = this.props;
    const targetNode = e.target as any;
    const clickedInside =
      this.outerRef.current && this.outerRef.current.contains(targetNode);
    if (this.outerRef && !clickedInside) {
      if (typeof onOutsideClick === 'function') {
        onOutsideClick();
      }
    }
  };
}
