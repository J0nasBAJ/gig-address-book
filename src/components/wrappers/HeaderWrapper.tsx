import * as React from 'react';
import styled from 'styled-components';

interface IHeaderWrapper extends React.HTMLProps<HTMLDivElement> {
  bg?: string;
  h?: number | string;
  jc?: 'start' | 'center' | 'space-around' | 'space-between' | 'space-evenly';
}

const HeaderWrapper = styled.div`
  top: 0;
  left: 0;
  right: 0;
  z-index: 100;
  display: flex;
  padding: 0 25px;
  position: fixed;
  flex-direction: row;
  align-items: center;
  background-color: ${(p: IHeaderWrapper) => p.bg || '#bfbfbf'};
  justify-content: ${(p: IHeaderWrapper) => p.jc || 'space-between'};
  height: ${(p: IHeaderWrapper) => (typeof p.h !== 'undefined' ? p.h : '50px')};
`;

export default HeaderWrapper;
