import React from 'react';
import styled from 'styled-components';

const ModalDimmer = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1000;
  position: absolute;
  background-color: rgba(0, 0, 0, 0.7);
`;

interface IModalWrapperProps {
  closeOnEsc?: boolean;
  onClose?: () => void;
}

export default class ModalWrapper extends React.PureComponent<
  IModalWrapperProps,
  {}
> {
  public componentWillMount() {
    const { closeOnEsc } = this.props;
    document.body.style.overflow = 'hidden';
    if (closeOnEsc) {
      document.addEventListener('keydown', this.onEsc, false);
    }
  }

  public componentWillUnmount() {
    const { closeOnEsc } = this.props;
    document.body.style.overflow = 'scroll';
    if (closeOnEsc) {
      document.removeEventListener('keydown', this.onEsc, false);
    }
  }

  public render() {
    const { children } = this.props;
    return <ModalDimmer children={children} />;
  }

  private onEsc = (e: DocumentEventMap['keydown']) => {
    const { onClose } = this.props;
    if (e.keyCode === 27 && typeof onClose === 'function') {
      onClose();
    }
  };
}
