import React from 'react';
import styled from 'styled-components';

interface IFlexWrapper extends React.HTMLProps<HTMLDivElement> {
  p?: string;
  m?: string;
  h?: number | string;
  flex?: number | string;
  fd?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  align?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'baseline'
    | 'stretch'
    | 'initial';
  justify?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
}

const FlexWrapper = styled.div`
  display: flex;
  align-items: ${(p: IFlexWrapper) => p.align};
  margin: ${(p: IFlexWrapper) => p.m || 'none'};
  padding: ${(p: IFlexWrapper) => p.p || 'none'};
  flex: ${(p: IFlexWrapper) => p.flex || '0 1 auto'}
  flex-direction: ${(p: IFlexWrapper) => p.fd || 'column'}
  justify-content: ${(p: IFlexWrapper) => p.justify || 'space-around'};
  height: ${(p: IFlexWrapper) => (typeof p.h !== 'undefined' ? p.h : '100%')};
`;

export default FlexWrapper;
