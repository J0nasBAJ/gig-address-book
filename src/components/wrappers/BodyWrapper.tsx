import * as React from 'react';
import styled from 'styled-components';

interface IBodyDiv extends React.HTMLProps<HTMLDivElement> {
  bg?: string;
}

const BodyDiv = styled.div`
  flex: 1;
  display: flex;
  min-width: 300px;
  min-height: 100vh;
  align-items: center;
  flex-direction: column;
  justify-content: flex-start;
  background-color: ${(p: IBodyDiv) => p.bg || '#ffffff'};
`;

export default BodyDiv;
