import * as React from 'react';
import styled from 'styled-components';
import { colors } from '../../helpers/colors';

interface IContactWrapper extends React.HTMLProps<HTMLLIElement> {
  o?: number;
  bg?: string;
  h?: number | string;
  jc?: 'start' | 'center' | 'space-around' | 'space-between' | 'space-evenly';
}

const ContactWrapper = styled.div`
  opacity: 0;
  width: 30%;
  flex-grow: 1;
  margin: 10px;
  min-width: 300px;
  overflow: hidden;
  border-radius: 15px;
  animation: fadeIn 0.5s ease-in both;
  box-shadow: 3px 4px 20px rgba(0, 0, 0, 0.2);
  opacity: ${(p: IContactWrapper) => (typeof p.o !== 'undefined' ? p.o : 1)};
  height: ${(p: IContactWrapper) =>
    typeof p.h !== 'undefined' ? p.h : '150px'};
  background-color: ${(p: IContactWrapper) =>
    typeof p.bg !== 'undefined' ? p.bg : colors.card};

  @keyframes fadeIn {
    0% {
      opacity: 0;
      transform: translate3d(0, -10%, 0);
    }
    80% {
      opacity: 0.8;
      transform: translate3d(0, 5%, 0);
    }
    100% {
      opacity: 1;
      transform: translate3d(0, 0%, 0);
    }
  }
`;

export default ContactWrapper;
