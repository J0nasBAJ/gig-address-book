import React from 'react';
import MenuButton from '../inputs/MenuButton';
import HeadWrapper from '../wrappers/HeaderWrapper';

import { config } from '../../config';
import { actions } from '../../state/actions';
import { colors } from '../../helpers/colors';

const menuItems = [
  {
    label: 'Contacts',
    link: '/contacts',
  },
  {
    label: 'Remove Contacts',
    link: '/remove-contacts',
  },
  {
    label: 'Load Demo Contacts',
    action: actions.contacts.syncDBContactsAction(),
  },
  {
    label: '404',
    link: '/non-existent-path',
  },
];

const Header = () => (
  <HeadWrapper bg={colors.header}>
    <MenuButton menuItems={menuItems} />
    <span style={{ fontSize: '10px', color: 'grey' }}>
      {config.REACT_APP_ENV}
    </span>
  </HeadWrapper>
);

export default Header;
