import React from 'react';
import styled from 'styled-components';
import { colors } from '../../helpers/colors';

interface IEditBar extends React.HTMLProps<HTMLDivElement> {
  p?: string;
  m?: string;
  bg?: string;
  br?: string;
  hbg?: string;
  h?: number | string;
  w?: number | string;
}

const EditBar = styled.div`
  overflow: hidden;
  margin: ${(p: IEditBar) => p.m || 'none'};
  padding: ${(p: IEditBar) => p.p || 'none'};
  border-radius: ${(p: IEditBar) => p.br || 'none'};
  width: ${(p: IEditBar) => (typeof p.w !== 'undefined' ? p.w : '100%')};
  height: ${(p: IEditBar) => (typeof p.h !== 'undefined' ? p.h : '100%')};
  background-color: ${(p: IEditBar) =>
    typeof p.bg !== 'undefined' ? p.bg : colors.primaryButton};

  &:hover {
    box-shadow: ${(p: IEditBar) => p.hbg || '2px 2px 20px rgb(235, 219, 137)'};
  }
`;

export default EditBar;
