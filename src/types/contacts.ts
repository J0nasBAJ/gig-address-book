import { constants } from '../state/constants';

export interface ICountry {
  countryName: string;
  countryCode: string;
}

export interface IContactPayload extends ICountry {
  email: string;
  lastName: string;
  firstName: string;
}

export interface IContact extends IContactPayload {
  id: string;
}

export interface ISetContactsPayload {
  contactIds: string[];
  contacts: { [id: string]: IContact };
}

export interface IContactsReduxState extends ISetContactsPayload {
  isOnSync: boolean;
}

export interface IAddNewContactAction {
  type: typeof constants.contacts.ADD_NEW_CONTACT;
  payload: IContactPayload;
}

export interface IUpdateContactAction {
  type: typeof constants.contacts.UPDATE_CONTACT;
  payload: IContact;
}

export interface ISetContactsAction {
  type: typeof constants.contacts.SET_CONTACTS;
  payload: ISetContactsPayload;
}

export interface IRemoveContactsAction {
  type: typeof constants.contacts.REMOVE_CONTACTS;
  id: string;
}

export interface IClearContactsStateAction {
  type: typeof constants.contacts.CLEAR_CONTACTS_STATE;
}

export interface ISyncContactsFromDBAction {
  type: typeof constants.contacts.SYNC_CONTACTS_FROM_DB;
}

export interface ISetContactsOnSyncFlagAction {
  type: typeof constants.contacts.SET_ON_SYNC_FLAG;
  flag: boolean;
}
