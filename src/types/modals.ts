import { constants } from '../state/constants';

export interface IModalsReduxState {
  addContactIsOpen: boolean;
}

export type ModalName = 'addContactIsOpen';

export interface ISetModalVisibleFlagActionPayload {
  visible: boolean;
  modalName: ModalName;
}

export interface ISetModalVisibleFlagAction {
  type: typeof constants.modals.SET_MODAL_VISIBLE_FLAG;
  payload: ISetModalVisibleFlagActionPayload;
}
