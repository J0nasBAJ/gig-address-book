import { Store } from 'redux';
import { History } from 'history';
import { IModalsReduxState } from './modals';
import { RouterState } from 'connected-react-router';
import { IContactsReduxState, ICountry } from './contacts';

export type IEnv = 'development' | 'production' | 'staging' | '';

export interface IAppReduxState {
  router: RouterState;
  modalState: IModalsReduxState;
  contactState: IContactsReduxState;
}

export interface IStoreConfig {
  history: History;
  store: Store<IAppReduxState>;
}

export interface IConfig {
  NODE_ENV: IEnv;
  REACT_APP_ENV: IEnv;
  PORT: number | string;
  FIREBASE: {
    apiKey?: string;
    authDomain?: string;
    databaseURL?: string;
    projectId?: string;
    storageBucket?: string;
    messagingSenderId?: string;
  };
}

export interface IGlobalCountries {
  countriesArray: ICountry[];
  countriesObject: { [countryName: string]: string };
}
