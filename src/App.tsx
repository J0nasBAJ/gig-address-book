import * as React from 'react';
import Routes from './containers/Routes';
import Header from './components/others/Header';
import BodyWrapper from './components/wrappers/BodyWrapper';
import AddContactModal from './components/modals/AddContactModal';

import { colors } from './helpers/colors';

class App extends React.Component {
  public render() {
    return (
      <BodyWrapper bg={colors.bg}>
        <Header />
        <Routes />
        <AddContactModal />
      </BodyWrapper>
    );
  }
}

export default App;
