import uuid from 'uuid';
import { IAppReduxState } from '../types/global';

const userIdKey = '@gigAddressBookUserId';
const storageKey = '@gigAddressBookReduxState';

export const loadPersistedState = () => {
  try {
    const rawState = localStorage.getItem(storageKey);
    return rawState ? JSON.parse(rawState) : undefined;
  } catch (error) {
    console.log(error);
    return undefined;
  }
};

export const persistState = (state: Partial<IAppReduxState>) => {
  try {
    const stringifiedState = JSON.stringify(state);
    localStorage.setItem(storageKey, stringifiedState);
  } catch (error) {
    console.log(error);
  }
};

export const getUserId = () => {
  const rawState = localStorage.getItem(userIdKey);
  const userId: string = rawState ? rawState : uuid.v1();
  localStorage.setItem(userIdKey, userId);
  return userId;
};
