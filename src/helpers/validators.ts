import { countries } from './staticData';
import { ICountry } from '../types/contacts';

export const validateEmail = (email: string) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export const validateCountry = (country: ICountry | null) => {
  if (country && country.countryCode && country.countryName) {
    return (
      countries.countriesObject[country.countryName.toLowerCase()] ===
      country.countryCode
    );
  } else {
    return false;
  }
};
