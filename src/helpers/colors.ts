export const colors = {
  buttonFont: '#323330',
  bg: 'rgb(48, 49, 48)',
  card: 'rgb(59, 60, 56)',
  input: 'rgb(40, 41, 38)',
  header: 'rgb(29, 31, 33)',
  primaryFont: 'rgb(241, 219, 108)',
  placeholder: 'rgb(113, 113, 113)',
  secondaryFont: 'rgb(255, 255, 255)',
  primaryButton: 'rgb(235, 219, 137)',
};
