import Countries from 'country-list';
import { keys, capitalize } from 'lodash';
import { IGlobalCountries } from '../types/global';

const countriesObject = Countries().getNameList();
const countriesArray = keys(countriesObject).map(k => ({
  countryName: capitalize(k),
  countryCode: countriesObject[k],
}));

export const countries: IGlobalCountries = {
  countriesArray,
  countriesObject,
};
