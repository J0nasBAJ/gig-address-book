import './database';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux';
import { configStore } from './state/store';
import { injectGlobal } from 'styled-components';
import { ConnectedRouter } from 'connected-react-router';

// tslint:disable:no-unused-expression
injectGlobal`
  body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
  }
`;
// tslint:enable:no-unused-expression

const { store, history } = configStore();
const root = document.getElementById('root') as HTMLElement;

const Application: JSX.Element = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(Application, root);
registerServiceWorker();
