import { IConfig, IEnv } from './types/global';

const config: IConfig = {
  PORT: process.env.PORT as number | string,
  NODE_ENV: process.env.NODE_ENV as IEnv,
  REACT_APP_ENV: process.env.REACT_APP_ENV as IEnv,
  FIREBASE: {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATA_BASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  },
};

export { config };
